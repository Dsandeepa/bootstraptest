<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet" />
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
    <!-- MDB -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.4.2/mdb.min.css" rel="stylesheet" />
</head>

<body>
    <main>
        <div class="container" style="height:500px;
        background-color:red;">
            <div class="row" style=" background-color: blue;">
                I am a first row
            </div>
            <div class="row" style=" background-color: lightblue;">
                I am a second row
            </div>

        </div>
        <div class="container">
            <div class="row">
                <div class="col">
                    1
                </div>
                <div class="col">
                    2
                </div>
                <div class="col">
                    3
                </div>
                <div class="col">
                    4
                </div>
                <div class="col">
                    5
                </div>
                <div class="col">
                    6
                </div>
                <div class="col">
                    7
                </div>
                <div class="col">
                    8
                </div>
                <div class="col">
                    9
                </div>
                <div class="col">
                    10
                </div>
                <div class="col">
                    11
                </div>
                <div class="col">
                    12
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-6">First column</div>
                <div class="col-6">Second column</div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-4">First column</div>
                <div class="col-8">Second column</div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-4 bg-success">First column</div>
                <div class="col-md-8 bg-info">Second column</div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-4">.col-md-4</div>
                <div class="col-md-4">.col-md-4</div>
                <div class="col-md-4">.col-md-4</div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-3 bg-warning">.col-md-3</div>
                <div class="col-md-6 bg-success">.col-md-6</div>
                <div class="col-md-3 bg-info">.col-md-3</div>
            </div>
        </div>
        <div class="container mt-5">
            <div class="row">
                <div class="col-8 bg-info">
                    col-md-8
                    <div class="row">
                        <div class="col-6 bg-warning">
                            col-md-6
                        </div>
                        <div class="col-6 bg-warning border-start">
                            col-md-6
                        </div>
                    </div>
                </div>
                <div class="col-4 bg-success">
                    col-md-8

                </div>
            </div>
        </div>
    </main>
</body>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.4.2/mdb.min.js"></script>

</html>