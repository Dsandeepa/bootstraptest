<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet" />
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
    <!-- MDB -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.4.2/mdb.min.css" rel="stylesheet" />
</head>

<body>
    <!--Main Navigation-->
    <header class="mb-5 mb-lg-7">

        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark" style="z-index:1;min-height:59px;">
            <!-- Container wrapper -->
            <div class="container">
                <!-- Toggle button -->
                <button class="navbar-toggler" type="button" data-mdb-toggle="collapse"
                    data-mdb-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-bars"></i>
                </button>

                <!-- Collapsible wrapper -->
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Navbar brand -->
                    <a class="navbar-brand mt-2 mt-lg-0" href="#">
                        <i class="fas fa-gem text-secondary"></i>
                    </a>
                    <!-- Left links -->
                    <!-- Left links -->
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link text-secondary fw-bold" href="#">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-secondary fw-bold" href="#">Projects</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-secondary fw-bold" href="#">Contact</a>
                        </li>
                    </ul>
                    <!-- Left links -->
                    <!-- Left links -->
                </div>
                <!-- Collapsible wrapper -->

                <!-- Right elements -->
                <!-- Right elements -->
                <div class="d-flex align-items-center">
                    <!-- Icon -->
                    <a class="text-secondary me-3" href="https://www.youtube.com/c/Mdbootstrap/videos" rel="nofollow"
                        target="_blank">
                        <i class="fab fa-youtube"></i>
                    </a>
                    <!-- Icon -->
                    <a class="text-secondary me-3" href="https://twitter.com/ascensus_mdb" rel="nofollow"
                        target="_blank">
                        <i class="fab fa-twitter"></i>
                    </a>
                    <!-- Icon -->
                    <a class="text-secondary me-3" href="https://github.com/mdbootstrap/mdb-ui-kit" rel="nofollow"
                        target="_blank">
                        <i class="fab fa-github"></i>
                    </a>

                </div>
                <!-- Right elements -->
                <!-- Right elements -->
            </div>
            <!-- Container wrapper -->
        </nav>
        <!-- Background image -->
        <div class="bg-image vh-100" style="
             margin-top: -58.59px;
             background-image: url('resources/018.jpg');">

            <!-- Mask -->
            <div class="mask" style="background-color: hsla(0, 0%, 0%, 0.6)">

                <!-- Container -->
                <div class="container d-flex justify-content-center align-items-center h-100">

                    <!-- Call to action -->
                    <div class="text-white text-center">
                        <h1 class="mb-3">Whoah, what a view!</h1>
                        <h5 class="mb-4">Learning web design is such an amazing thing</h5>
                        <a class="btn btn-secondary btn btn-rounded" href="#" role="button">
                            Learn with me<i class="fas fa-graduation-cap ms-2"></i>
                        </a>
                    </div>

                </div>

            </div>

        </div>
        <!-- Navbar -->

    </header>
    <main>
        <div class="container">
            <section class="mb-5 mb-lg-10">
                <div class="row gx-xl-5">

                    <div class="col-lg-7 mb-5 mb-lg-0">
                        <!-- Carousel wrapper -->
                        <div id="carouselBasicExample" class="carousel slide carousel-fade" data-mdb-ride="carousel">
                            <!-- Indicators -->
                            <div class="carousel-indicators">
                                <button type="button" data-mdb-target="#carouselBasicExample" data-mdb-slide-to="0"
                                    class="active" aria-current="true" aria-label="Slide 1"></button>
                                <button type="button" data-mdb-target="#carouselBasicExample" data-mdb-slide-to="1"
                                    aria-label="Slide 2"></button>
                                <button type="button" data-mdb-target="#carouselBasicExample" data-mdb-slide-to="2"
                                    aria-label="Slide 3"></button>
                                <button type="button" data-mdb-target="#carouselBasicExample" data-mdb-slide-to="3"
                                    aria-label="Slide 3"></button>
                            </div>

                            <!-- Inner -->
                            <div class="carousel-inner rounded-6 shadow-4-strong">
                                <!-- Single item -->
                                <div class="carousel-item active">
                                    <img src="resources/1 (1).jpg" class="d-block w-100" alt="Sunset Over the City" />
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>First slide label</h5>
                                        <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                                    </div>
                                </div>

                                <!-- Single item -->
                                <div class="carousel-item">
                                    <img src="resources/1 (2).jfif" class="d-block w-100" alt="Canyon at Nigh" />
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Second slide label</h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    </div>
                                </div>

                                <!-- Single item -->
                                <div class="carousel-item">
                                    <img src="resources/1 (4).jpg" class="d-block w-100"
                                        alt="Cliff Above a Stormy Sea" />
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Third slide label</h5>
                                        <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <img src="resources/1 (5).jpg" class="d-block w-100"
                                        alt="Cliff Above a Stormy Sea" />
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Third slide label</h5>
                                        <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                                    </div>
                                </div>
                            </div>
                            <!-- Inner -->

                            <!-- Controls -->
                            <button class="carousel-control-prev" type="button" data-mdb-target="#carouselBasicExample"
                                data-mdb-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Previous</span>
                            </button>
                            <button class="carousel-control-next" type="button" data-mdb-target="#carouselBasicExample"
                                data-mdb-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Next</span>
                            </button>
                        </div>
                        <!-- Carousel wrapper -->
                    </div>

                    <div class="col-lg-5 mb-5 mb-lg-0">
                        <h3 class="fw-bold mb-4">Details</h3>

                        <p>This beginner-friendly, example-based course will guide you through the essential knowledge
                            of MDB
                            ecosystem.</p>

                        <p class="mb-2"><strong>What will you learn:</strong></p>

                        <ul class="list-unstyled">
                            <li><i class="fas fa-check text-success me-2"></i>Bootstrap</li>
                            <li><i class="fas fa-check text-success me-2"></i>MDBootstrap</li>
                            <li><i class="fas fa-check text-success me-2"></i>UI & UX design</li>
                            <li><i class="fas fa-check text-success me-2"></i>Responsive web design</li>
                        </ul>

                        <a class="btn btn-link btn-rounded" href="#" role="button" data-mdb-ripple-color="primary">Learn
                            more</a>

                    </div>

                </div>
            </section>
        </div>
        <div class="container d-flex justify-content-center align-items-center mb-7">
            <div class="row">
                <div class="col-12">
                    <i class="fas fa-gem fa-xs"></i>
                    <i class="fas fa-gem fa-sm"></i>
                    <i class="fas fa-gem fa-lg"></i>
                    <i class="fas fa-gem fa-2x"></i>
                    <i class="fas fa-gem fa-3x"></i>
                    <i class="fas fa-gem fa-4x"></i>
                    <i class="fas fa-gem fa-5x"></i>
                    <i class="fas fa-gem fa-6x"></i>
                    <i class="fas fa-gem fa-7x"></i>
                    <i class="fas fa-gem fa-8x"></i>
                    <i class="fas fa-gem fa-9x"></i>
                    <i class="fas fa-gem fa-10x"></i>
                </div>
                <div class="col-12">
                    <i class="fas fa-gem fa-3x text-primary me-2"></i>
                    <i class="fas fa-gem fa-3x text-secondary me-2"></i>
                    <i class="fas fa-gem fa-3x text-success me-2"></i>
                    <i class="fas fa-gem fa-3x text-danger me-2"></i>
                    <i class="fas fa-gem fa-3x text-warning me-2"></i>
                    <i class="fas fa-gem fa-3x text-info me-2"></i>
                    <i class="fas fa-gem fa-3x text-dark me-2"></i>
                </div>
            </div>

        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <ol>
                        <li>Lorem</li>
                        <li>Ipsum</li>
                        <li>Dolor</li>
                    </ol>
                    <ul>
                        <li>Lorem</li>
                        <li>Ipsum</li>
                        <li>Dolor</li>
                    </ul>

                    <ul class="list-unstyled">
                        <li class="mb-1"><i class="fas fa-question-circle me-2 text-warning"></i>What is the latest
                            version?</li>
                        <li class="mb-1"><i class="fas fa-question-circle me-2 text-warning"></i>What are the changes in
                            the latest
                            version compared to the previous one?</li>
                        <li class="mb-1"><i class="fas fa-question-circle me-2 text-warning"></i>Is the latest version
                            stable already?
                        </li>
                    </ul>
                    <ul class="list-unstyled">
                        <li class="mb-1"><i class="fas fa-long-arrow-alt-right me-2 text-info"></i>Now it is even
                            easier, lighter and better</li>
                        <li class="mb-1"><i class="fas fa-long-arrow-alt-right me-2 text-info"></i>Improved
                            documentation</li>
                        <li class="mb-1"><i class="fas fa-long-arrow-alt-right me-2 text-info"></i>Improved modularity
                        </li>
                    </ul>
                    <ul class="list-unstyled">
                        <li class="mb-1"><i class="fas fa-check-circle me-2 text-success"></i>Hundreds of additional
                            quality components</li>
                        <li class="mb-1"><i class="fas fa-check-circle me-2 text-success"></i>Much better design</li>
                        <li class="mb-1"><i class="fas fa-check-circle me-2 text-success"></i>Integration with
                            TypeScript</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="col-lg-5">

                <h3 class="fw-bold mb-4">Details</h3>

                <p>This beginner-friendly, example-based course will guide you through the essential knowledge of MDB
                    ecosystem.</p>

                <p class="mb-2"><strong>What will you learn:</strong></p>

                <ul class="list-unstyled">
                    <li><i class="fas fa-check text-success me-2"></i>Bootstrap</li>
                    <li><i class="fas fa-check text-success me-2"></i>MDBootstrap</li>
                    <li><i class="fas fa-check text-success me-2"></i>UI & UX design</li>
                    <li><i class="fas fa-check text-success me-2"></i>Responsive web design</li>
                </ul>

                <a class="btn btn-link btn-rounded" href="#" role="button" data-mdb-ripple-color="primary">Learn
                    more</a>

            </div>
        </div>
        <div class="container">
            <div class="row gx-xl-7">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk
                                of the
                                card's content.</p>
                            <button type="button" class="btn btn-primary">Button</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card">
                        <img src="resources/1 (4).jpg" class="card-img-top" alt="Fissure in Sandstone" />
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk
                                of the
                                card's content.</p>
                            <a href="#!" class="btn btn-primary">Button</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <!-- Section: Pricing -->
                <section class="mb-5 mt-5 mb-lg-10">

                    <h3 class="fw-bold text-center mb-5">Pricing</h3>

                    <div class="row gx-xl-5">
                        <div class="col-lg-4 col-md-12 mb-4 mb-lg-0">

                            <div class="card">
                                <div class="card-header text-center pt-4">
                                    <p class="text-uppercase">
                                        <strong>Basic</strong>
                                    </p>

                                    <h3 class="mb-4">
                                        <strong>$ 129</strong>
                                        <small class="text-muted" style="font-size: 16px">/year</small>
                                    </h3>

                                    <button type="button" class="btn btn-secondary btn-rounded w-100 mb-3">
                                        Buy
                                    </button>

                                </div>
                                <div class="card-body">

                                    <ol class="list-unstyled mb-0">
                                        <li class="mb-3">
                                            <i class="fas fa-check text-success me-3"></i>Unlimited
                                            updates
                                        </li>
                                        <li class="mb-3">
                                            <i class="fas fa-check text-success me-3"></i>Git repository
                                        </li>
                                        <li class="mb-3">
                                            <i class="fas fa-check text-success me-3"></i>npm
                                            installation
                                        </li>
                                    </ol>

                                </div>
                            </div>

                        </div>

                        <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">

                            <div class="card h-100">
                                <div class="card-header text-center pt-4">
                                    <p class="text-uppercase">
                                        <strong>Advanced</strong>
                                    </p>

                                    <h3 class="mb-4">
                                        <strong>$ 299</strong>
                                        <small class="text-muted" style="font-size: 16px">/year</small>
                                    </h3>

                                    <button type="button" class="btn btn-primary btn-rounded w-100 mb-3">
                                        Buy
                                    </button>
                                </div>
                                <div class="card-body">
                                    <ol class="list-unstyled mb-0">
                                        <li class="mb-3">
                                            <i class="fas fa-check text-success me-3"></i>Unlimited
                                            updates
                                        </li>
                                        <li class="mb-3">
                                            <i class="fas fa-check text-success me-3"></i>Git repository
                                        </li>
                                        <li class="mb-3">
                                            <i class="fas fa-check text-success me-3"></i>npm
                                            installation
                                        </li>
                                        <li class="mb-3">
                                            <i class="fas fa-check text-success me-3"></i>Code examples
                                        </li>
                                        <li class="mb-3">
                                            <i class="fas fa-check text-success me-3"></i>Premium
                                            snippets
                                        </li>
                                    </ol>
                                </div>
                            </div>

                        </div>

                        <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">

                            <div class="card h-100">
                                <div class="card-header text-center pt-4">
                                    <p class="text-uppercase">
                                        <strong>Enterprise</strong>
                                    </p>
                                    <h3 class="mb-4">
                                        <strong>$ 499</strong>
                                        <small class="text-muted" style="font-size: 16px">/year</small>
                                    </h3>

                                    <button type="button" class="btn btn-secondary btn-rounded w-100 mb-3">
                                        Buy
                                    </button>
                                </div>
                                <div class="card-body">
                                    <ol class="list-unstyled mb-0">
                                        <li class="mb-3">
                                            <i class="fas fa-check text-success me-3"></i>Unlimited
                                            updates
                                        </li>
                                        <li class="mb-3">
                                            <i class="fas fa-check text-success me-3"></i>Git repository
                                        </li>
                                        <li class="mb-3">
                                            <i class="fas fa-check text-success me-3"></i>npm
                                            installation
                                        </li>
                                        <li class="mb-3">
                                            <i class="fas fa-check text-success me-3"></i>Code examples
                                        </li>
                                        <li class="mb-3">
                                            <i class="fas fa-check text-success me-3"></i>Premium
                                            snippets
                                        </li>
                                        <li class="mb-3">
                                            <i class="fas fa-check text-success me-3"></i>Premium
                                            support
                                        </li>
                                        <li class="mb-3">
                                            <i class="fas fa-check text-success me-3"></i>Drag&amp;Drop
                                            builder
                                        </li>
                                    </ol>
                                </div>
                            </div>

                        </div>
                    </div>

                </section>
            </div>
            <div class="row mb-5">

                <div class="col order-2 bg-info">First</div>
                <div class="col order-3 bg-warning">Second</div>
                <div class="col order-1 bg-success">Third</div>

            </div>
            <div class="row mb-5">
                <h1>Example heading <span class="badge badge-primary">New</span></h1>
                <h2>Example heading <span class="badge badge-primary">New</span></h2>
                <h3>Example heading <span class="badge badge-primary">New</span></h3>
                <h4>Example heading <span class="badge badge-primary">New</span></h4>
                <h5>Example heading <span class="badge badge-primary">New</span></h5>
                <h6>Example heading <span class="badge badge-primary">New</span></h6>
            </div>
            <div class="row">
                <span class="badge badge-primary">Primary</span>
                <span class="badge badge-secondary">Secondary</span>
                <span class="badge badge-success">Success</span>
                <span class="badge badge-danger">Danger</span>
                <span class="badge badge-warning">Warning</span>
                <span class="badge badge-info">Info</span>
                <span class="badge badge-light">Light</span>
                <span class="badge badge-dark">Dark</span>
                <div class="badge badge-primary p-3 rounded-4">
                    <i class="fas fa-chart-pie"></i>
                    <div class="d-flex align-items-start">
                        <div class="p-3 badge-primary rounded-4">
                            <i class="fas fa-headset fa-lg fa-fw"></i>
                        </div>
                        <div class="ms-4">
                            <p class="fw-bold mb-1">Technical support</p>
                            <p class="text-muted mb-0">support@example.com</p>
                            <p class="text-muted mb-0">+1 234-567-89</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Section: Pricing -->
        </div>
        <div class="container">
            <!-- Section: Contact -->
            <section class="mb-5 mb-lg-10 mt-5">

                <h3 class="fw-bold text-center mb-5">Contact us</h3>

                <div class="row gx-xl-5">

                    <div class="col-lg-5 mb-4 mb-lg-0">

                        <form>
                            <!-- Name input -->
                            <div class="form-outline mb-4">
                                <input type="text" id="form4Example1" class="form-control" />
                                <label class="form-label" for="form4Example1">Name</label>
                            </div>

                            <!-- Email input -->
                            <div class="form-outline mb-4">
                                <input type="email" id="form4Example2" class="form-control" />
                                <label class="form-label" for="form4Example2">Email address</label>
                            </div>

                            <!-- Message input -->
                            <div class="form-outline mb-4">
                                <textarea class="form-control" id="form4Example3" rows="4"></textarea>
                                <label class="form-label" for="form4Example3">Message</label>
                            </div>

                            <!-- Checkbox -->
                            <div class="form-check d-flex justify-content-center mb-4">
                                <input class="form-check-input me-2" type="checkbox" value="" id="form4Example4"
                                    checked />
                                <label class="form-check-label" for="form4Example4">
                                    Send me a copy of this message
                                </label>
                            </div>

                            <!-- Submit button -->
                            <button type="submit" class="btn btn-primary btn-block mb-4">Send</button>
                        </form>

                    </div>

                    <div class="col-lg-7 mb-4 mb-lg-0">

                        <div class="row gx-lg-5">
                            <div class="col-md-6 mb-4 mb-md-5">
                                <div class="d-flex align-items-start">
                                    <div class="p-3 badge-primary rounded-4">
                                        <i class="fas fa-headset fa-lg fa-fw"></i>
                                    </div>
                                    <div class="ms-4">
                                        <p class="fw-bold mb-1">Technical support</p>
                                        <p class="text-muted mb-0">support@example.com</p>
                                        <p class="text-muted mb-0">+1 234-567-89</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 mb-4 mb-md-5">
                                <div class="d-flex align-items-start">
                                    <div class="p-3 badge-primary rounded-4">
                                        <i class="fas fa-dollar-sign fa-lg fa-fw"></i>
                                    </div>
                                    <div class="ms-4">
                                        <p class="fw-bold mb-1">Sales questions</p>
                                        <p class="text-muted mb-0">sales@example.com</p>
                                        <p class="text-muted mb-0">+1 234-567-88</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 mb-4 mb-md-5">
                                <div class="d-flex align-items-start">
                                    <div class="p-3 badge-primary rounded-4">
                                        <i class="fas fa-newspaper fa-lg fa-fw"></i>
                                    </div>
                                    <div class="ms-4">
                                        <p class="fw-bold mb-1">Press</p>
                                        <p class="text-muted mb-0">press@example.com</p>
                                        <p class="text-muted mb-0">+1 234-567-87</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 mb-4 mb-md-5">
                                <div class="d-flex align-items-start">
                                    <div class="p-3 badge-primary rounded-4">
                                        <i class="fas fa-bug fa-lg fa-fw"></i>
                                    </div>
                                    <div class="ms-4">
                                        <p class="fw-bold mb-1">Bug report</p>
                                        <p class="text-muted mb-0">bugs@example.com</p>
                                        <p class="text-muted mb-0">+1 234-567-86</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </section>
            <!-- Section: Contact -->
        </div>
    </main>
    <footer class="text-center text-white" style="background-color: #f1f1f1;">
  <!-- Grid container -->
  <div class="container pt-4">
    <!-- Section: Social media -->
    <section class="mb-4">
      <!-- Facebook -->
      <a
        class="btn btn-link btn-floating btn-lg text-dark m-1"
        href="#!"
        role="button"
        data-mdb-ripple-color="dark"
        ><i class="fab fa-facebook-f"></i
      ></a>

      <!-- Twitter -->
      <a
        class="btn btn-link btn-floating btn-lg text-dark m-1"
        href="#!"
        role="button"
        data-mdb-ripple-color="dark"
        ><i class="fab fa-twitter"></i
      ></a>

      <!-- Google -->
      <a
        class="btn btn-link btn-floating btn-lg text-dark m-1"
        href="#!"
        role="button"
        data-mdb-ripple-color="dark"
        ><i class="fab fa-google"></i
      ></a>

      <!-- Instagram -->
      <a
        class="btn btn-link btn-floating btn-lg text-dark m-1"
        href="#!"
        role="button"
        data-mdb-ripple-color="dark"
        ><i class="fab fa-instagram"></i
      ></a>

      <!-- Linkedin -->
      <a
        class="btn btn-link btn-floating btn-lg text-dark m-1"
        href="#!"
        role="button"
        data-mdb-ripple-color="dark"
        ><i class="fab fa-linkedin"></i
      ></a>
      <!-- Github -->
      <a
        class="btn btn-link btn-floating btn-lg text-dark m-1"
        href="#!"
        role="button"
        data-mdb-ripple-color="dark"
        ><i class="fab fa-github"></i
      ></a>
    </section>
    <!-- Section: Social media -->
  </div>
  <!-- Grid container -->

  <!-- Copyright -->
  <div class="text-center text-dark p-3" style="background-color: rgba(0, 0, 0, 0.2);">
    © 2020 Copyright:
    <a class="text-dark" href="https://mdbootstrap.com/">MDBootstrap.com</a>
  </div>
  <!-- Copyright -->
</footer>
    <!--Main Navigation-->
</body>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.4.2/mdb.min.js"></script>

</html>