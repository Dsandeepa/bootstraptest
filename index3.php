<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet" />
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
    <!-- MDB -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.4.2/mdb.min.css" rel="stylesheet" />
</head>

<body>
    <!--Main Navigation-->
    <header>
        <!-- Navbar -->
        <nav id="main-navbar"
            class="navbar navbar-expand-lg navbar-light bg-light fixed-top navbar-before-scroll shadow-0">
            <!-- Container wrapper -->
            <div class="container-fluid">
                <!-- Toggle button -->
                <button class="navbar-toggler" type="button" data-mdb-toggle="collapse"
                    data-mdb-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-bars"></i>
                </button>

                <!-- Collapsible wrapper -->
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <a class="navbar-brand me-1" href="#"><img src="resources/SC Logo Selected 1.png" height="20px"
                                alt="Logo" loading="lazy" /></a>
                        <li class="nav-item">
                            <a class="nav-link text-dark" href="#!">Projects</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-dark" href="#!">About me</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-dark" href="#!">Testimonials</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-dark" href="#!">Contact</a>
                        </li>
                    </ul>

                    <ul class="navbar-nav flex-row">
                        <!-- Icons -->
                        <li class="nav-item">
                            <a class="nav-link pe-2" href="#!">
                                <i class="fab fa-youtube"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link px-2" href="#!">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link px-2" href="#!">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link ps-2" href="#!">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- Collapsible wrapper -->

            </div>
            <!-- Container wrapper -->
        </nav>
        <!-- Navbar -->
    </header>
    <!--Main Navigation-->
    <main>
        <section class="">

            <div class="container-fluid px-0">
                <div class="row g-0">
                    <div class="col-lg-6 vh-100 bg-danger">
                        <div class="vh-100 bg-success"></div>
                    </div>
                    <div class="col-lg-6 vh-100 bg-primary">
                        <div class="vh-100 bg-warning"></div>
                    </div>
                </div>
            </div>

        </section>
        <section class="mb-5">

            <div class="container-fluid px-0">
                <div class="row g-0">
                    <div class="col-lg-6 vh-100 d-flex flex-column justify-content-center align-items-center">
                        <div class="">
                            <h2 class="display-4">John Doe</h2>
                            <h1 class="display-2 fw-bold text-uppercase">Web developer</h1>
                        </div>

                    </div>
                    <div class="col-lg-6 vh-100">
                        <div id="carouselBasicExample" class="carousel slide carousel-fade" data-mdb-ride="carousel">
                            <!-- Indicators -->
                            <div class="carousel-indicators">
                                <button type="button" data-mdb-target="#carouselBasicExample" data-mdb-slide-to="0"
                                    class="active rounded-circle" style="width:7px; height:7px;" aria-current="true"
                                    aria-label="Slide 1"></button>
                                <button type="button" data-mdb-target="#carouselBasicExample" data-mdb-slide-to="1"
                                    aria-label="Slide 2" class=" rounded-circle"
                                    style="width:7px; height:7px;"></button>
                                <button type="button" data-mdb-target="#carouselBasicExample" data-mdb-slide-to="2"
                                    aria-label="Slide 3" class=" rounded-circle"
                                    style="width:7px; height:7px;"></button>
                            </div>

                            <!-- Inner -->
                            <div class="carousel-inner shadow-5-strong" style=" border-bottom-left-radius:4rem;">
                                <!-- Single item -->
                                <div class="carousel-item active">
                                    <img src="resources/1 (2).jfif" class="d-block vh-100 vw-100 object-cover"
                                        alt="Sunset Over the City" />
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>First slide label</h5>
                                        <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                                    </div>
                                </div>

                                <!-- Single item -->
                                <div class="carousel-item">
                                    <img src="resources/1 (1).jpg" class="d-block vh-100 vw-100 object-cover"
                                        alt="Canyon at Nigh" />
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Second slide label</h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    </div>
                                </div>

                                <!-- Single item -->
                                <div class="carousel-item">
                                    <img src="resources/018.jpg" class="d-block vh-100 vw-100 object-cover"
                                        alt="Cliff Above a Stormy Sea" />
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Third slide label</h5>
                                        <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                                    </div>
                                </div>
                            </div>
                            <!-- Inner -->

                            <!-- Controls -->
                            <button class="carousel-control-prev" type="button" data-mdb-target="#carouselBasicExample"
                                data-mdb-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Previous</span>
                            </button>
                            <button class="carousel-control-next" type="button" data-mdb-target="#carouselBasicExample"
                                data-mdb-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Next</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <!-- Section: My projects -->

        <section class="mb-10 text-center">
            <div class="container">
                <h2 class="fw-bold mb-7 text-center">My projects</h2>

                <div class="row gx-lg-5">

                    <!-- First column -->
                    <div class="col-lg-4 col-md-12 mb-6 mb-lg-0">

                        <div class="card rounded-6 h-100" data-mdb-toggle="animation"
                            data-mdb-animation-start="onScroll" data-mdb-animation="fade-in"
                            data-mdb-animation-show-on-load="false" data-mdb-animation-delay="400">
                            <div class="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                                <img src="https://mdbootstrap.com/img/new/textures/small/148.jpg" class="img-fluid" />
                                <a href="#!">
                                    <div class="mask" style="background-color: rgba(251, 251, 251, 0.15);"></div>
                                </a>
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">AI engine</h5>
                                <p class="text-muted">
                                    <small>Finished <u>13.09.2021</u> for
                                        <a href="" class="text-dark">Techify</a></small>
                                </p>
                                <p class="card-text">
                                    Ut pretium ultricies dignissim. Sed sit amet mi eget urna
                                    placerat vulputate. Ut vulputate est non quam dignissim
                                    elementum. Donec a ullamcorper diam.
                                </p>
                                <a href="#!" class="btn btn-secondary btn-rounded">Read more</a>
                            </div>
                        </div>

                    </div>
                    <!-- First column -->

                    <!-- Second column -->
                    <div class="col-lg-4 mb-6 mb-lg-0">

                        <div class="card">
                            <div class="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                                <img src="https://mdbootstrap.com/img/new/textures/small/38.jpg" class="img-fluid" />
                                <a href="#!">
                                    <div class="mask" style="background-color: rgba(251, 251, 251, 0.15);"></div>
                                </a>
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">Balanced design</h5>
                                <p class="text-muted">
                                    <small>Finished <u>12.01.2022</u> for
                                        <a href="" class="text-dark">Rubicon</a></small>
                                </p>
                                <p class="card-text">
                                    Suspendisse in volutpat massa. Nulla facilisi. Sed aliquet
                                    diam orci, nec ornare metus semper sed. Integer volutpat
                                    ornare erat sit amet rutrum. Ut vulputate est non quam.
                                </p>
                                <a href="#!" class="btn btn-secondary btn-rounded">Read more</a>
                            </div>
                        </div>

                    </div>
                    <!-- Second column -->

                    <!-- Third column -->
                    <div class="col-lg-4 mb-6 mb-lg-0">

                        <div class="card">
                            <div class="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                                <img src="https://mdbootstrap.com/img/new/textures/small/55.jpg" class="img-fluid" />
                                <a href="#!">
                                    <div class="mask" style="background-color: rgba(251, 251, 251, 0.15);"></div>
                                </a>
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">Metaverse 2.0</h5>
                                <p class="text-muted">
                                    <small>Finished <u>10.11.2022</u> for
                                        <a href="" class="text-dark">Venom Tech</a></small>
                                </p>
                                <p class="card-text">
                                    Curabitur tristique, mi a mollis sagittis, metus felis mattis
                                    arcu, non vehicula nisl dui quis diam. Mauris ut risus eget
                                    massa volutpat feugiat. Donec.
                                </p>
                                <a href="#!" class="btn btn-secondary btn-rounded">Read more</a>
                            </div>
                        </div>

                    </div>
                    <!-- Third column -->
                </div>
            </div>

        </section>
        <!-- Section: About me -->
        <section class="mb-10">

            <div class="container">

                <div class="row gx-0 align-items-center d-flex">

                    <!-- First column -->
                    <div class="col-lg-6 mb-5 mb-lg-0">

                        <div class="card rounded-7 me-lg-n5">
                            <div class="card-body p-lg-5 shadow-5">
                                <h2 class="fw-bold">
                                    <span class="text-primary">John Doe</span>
                                </h2>
                                <p class="fw-bold"><em>“Design is intelligence made visible.”</em></p>

                                <p class="text-muted mb-4">
                                    Nunc tincidunt vulputate elit. Mauris varius purus malesuada
                                    neque iaculis malesuada. Aenean gravida magna orci, non
                                    efficitur est porta id. Donec magna diam.
                                </p>
                                <p class="text-muted">
                                    Ad, at blanditiis quaerat laborum officia incidunt
                                    cupiditate dignissimos voluptates eius aliquid minus
                                    praesentium! Perferendis et totam ipsum ex aut earum libero
                                    accusamus voluptas quod numquam saepe, consequuntur nihil
                                    quia tenetur consequatur. Quis, explicabo deserunt quasi
                                    assumenda ea maiores nulla, et dolor saepe praesentium natus
                                    error reiciendis voluptas iste. Esse, laudantium ipsum
                                    animi, quos voluptatibus atque vero repellat sit eligendi
                                    autem maiores quasi cum aperiam. Aperiam rerum culpa
                                    accusantium, ducimus deserunt aliquid alias vitae quasi
                                    corporis placeat vel maiores explicabo commodi!
                                </p>
                            </div>
                        </div>

                    </div>
                    <!-- First column -->

                    <!-- Second column -->
                    <div class="col-lg-6 mb-5 mb-lg-0">

                        <img src="https://mdbootstrap.com/img/new/standard/people/272.jpg"
                            class="w-100 rounded-7 shadow-4" alt="" />

                    </div>
                    <!-- Second column -->

                </div>

            </div>

        </section>
        <!-- Section: Testimonials -->
        <section class="mb-10 text-center">
            <div class="container">
                <h2 class="fw-bold mb-7 text-center">Happy clients</h2>

                <div class="row gx-lg-5">

                    <!-- First column -->
                    <div class="col-lg-4 mb-5 mb-lg-0">

                        <div>
                            <div class="rounded-7 p-4" style="background-color: hsl(218, 62.2%, 95%)">
                                <p class="text-muted mt-4 mb-2">Project Manager at Spotify</p>
                                <p class="h5 mb-4 text-primary">Garry Lindman</p>
                                <p class="pb-4 mb-4">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis molestias quidem
                                    itaque earum tempora distinctio soluta ut, eius, impedit porro iure praesentium
                                    ratione possimus quos suscipit, ratione nostrum cum odit.
                                </p>
                            </div>
                            <img src="https://mdbootstrap.com/img/new/avatars/22.jpg" class="w-100 rounded-7 mt-n5"
                                style="max-width: 100px" alt="Avatar" />
                        </div>

                    </div>
                    <!-- First column -->

                    <!-- Second column -->
                    <div class="col-lg-4 mb-5 mb-lg-0">

                        <div>
                            <div class="rounded-7 p-4" style="background-color: hsl(218, 62.2%, 95%)">
                                <p class="text-muted mt-4 mb-2">CEO at Reddit</p>
                                <p class="h5 mb-4 text-primary">Lisa Montessori</p>
                                <p class="pb-4 mb-4">
                                    Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum
                                    soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime
                                    placeat facere possimus, omnis voluptas assumenda est omnis.
                                </p>
                            </div>
                            <img src="https://mdbootstrap.com/img/new/avatars/11.jpg" class="w-100 rounded-7 mt-n5"
                                style="max-width: 100px" alt="Avatar" />
                        </div>

                    </div>
                    <!-- Second column -->

                    <!-- Third column -->
                    <div class="col-lg-4">

                        <div>
                            <div class="rounded-7 p-4" style="background-color: hsl(218, 62.2%, 95%)">
                                <p class="text-muted mt-4 mb-2">Senior Product Designer at Twitter</p>
                                <p class="h5 mb-4 text-primary">Ozzy McRyan</p>
                                <p class="pb-4 mb-4">
                                    Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe
                                    eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque
                                    earum rerum hic tenetur a sapiente delectus ut aut reiciendis.
                                </p>
                            </div>
                            <img src="https://mdbootstrap.com/img/new/avatars/25.jpg" class="w-100 rounded-7 mt-n5"
                                style="max-width: 100px" alt="Avatar" />
                        </div>

                    </div>
                    <!-- Third column -->

                </div>
            </div>
        </section>
        <section class="text-center mb-5">
            <div class="row">
                <div class="col-lg-3 col-md-6 mb-5 mb-md-5 mb-lg-0 position-relative">
                    <i class="fas fa-cubes fa-3x text-primary mb-4"></i>
                    <h5 class="text-primary fw-bold mb-3">5000+</h5>
                    <h6 class="fw-normal mb-0">Components</h6>
                    <div class="vr vr-blurry position-absolute my-0 h-100 d-none d-md-block top-0 end-0"></div>
                </div>

                <div class="col-lg-3 col-md-6 mb-5 mb-md-5 mb-lg-0 position-relative">
                    <i class="fas fa-layer-group fa-3x text-primary mb-4"></i>
                    <h5 class="text-primary fw-bold mb-3">490+</h5>
                    <h6 class="fw-normal mb-0">Design blocks</h6>
                    <div class="vr vr-blurry position-absolute my-0 h-100 d-none d-md-block top-0 end-0"></div>
                </div>

                <div class="col-lg-3 col-md-6 mb-5 mb-md-0 position-relative">
                    <i class="fas fa-image fa-3x text-primary mb-4"></i>
                    <h5 class="text-primary fw-bold mb-3">100+</h5>
                    <h6 class="fw-normal mb-0">Templates</h6>
                    <div class="vr vr-blurry position-absolute my-0 h-100 d-none d-md-block top-0 end-0"></div>
                </div>

                <div class="col-lg-3 col-md-6 mb-5 mb-md-0 position-relative">
                    <i class="fas fa-plug fa-3x text-primary mb-4"></i>
                    <h5 class="text-primary fw-bold mb-3">28</h5>
                    <h6 class="fw-normal mb-0">Plugins</h6>
                </div>
            </div>
        </section>
        <div class="container">
            <!-- First column -->
            <div class="col-lg-6 mb-5 mb-lg-0">

                <div style="background: hsla(0, 0%, 100%, 0.55);
backdrop-filter: blur(30px);
z-index: 1;" class="card rounded-7 me-lg-n5">
                    <div class="card-body p-lg-5 shadow-5">
                        <form>
                            <!-- Name input -->
                            <div class="form-outline mb-4">
                                <input type="text" id="form4Example1" class="form-control" />
                                <label class="form-label" for="form4Example1">Name</label>
                            </div>

                            <!-- Email input -->
                            <div class="form-outline mb-4">
                                <input type="email" id="form4Example2" class="form-control" />
                                <label class="form-label" for="form4Example2">Email address</label>
                            </div>

                            <!-- Message input -->
                            <div class="form-outline mb-4">
                                <textarea class="form-control" id="form4Example3" rows="4"></textarea>
                                <label class="form-label" for="form4Example3">Message</label>
                            </div>

                            <!-- Checkbox -->
                            <div class="form-check d-flex justify-content-center mb-4">
                                <input class="form-check-input me-2" type="checkbox" value="" id="form4Example4"
                                    checked />
                                <label class="form-check-label" for="form4Example4">
                                    Send me a copy of this message
                                </label>
                            </div>

                            <!-- Submit button -->
                            <button type="submit" class="btn btn-primary btn-block mb-4">Send</button>
                        </form>
                    </div>
                </div>

            </div>
            <!-- Second column -->
            <div class="ratio ratio-1x1">
                <iframe class="shadow-3-strong rounded-7"
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d96813.17497894862!2d-73.97484803586903!3d40.68692922859912!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c2588f046ee661%3A0xa0b3281fcecc08c!2sManhattan%2C%20New%20York%2C%20NY%2C%20USA!5e0!3m2!1sen!2spl!4v1671718528728!5m2!1sen!2spl"
                    allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
            <!-- Second column -->
            <!-- First column -->
        </div>
        <!-- Section: Testimonials -->
        <!-- Section: About me -->
        <!-- Section: My projects -->

        <!-- Carousel wrapper -->

        <!-- Carousel wrapper -->

    </main>
</body>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.4.2/mdb.min.js"></script>

</html>